-- Production.brand definition

CREATE TABLE `brand` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Production.product definition

CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `art_number` int NOT NULL,
  `brand_id` int NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO Production.product
(art_number, brand_id, description, name)
VALUES(11, 1, 'rtert', 'tu,mu');
INSERT INTO Production.product
(art_number, brand_id, description, name)
VALUES(22, 2, 'qweqw', 'netnbe');
INSERT INTO Production.product
(art_number, brand_id, description, name)
VALUES(33, 3, 'axcax', 'vrqevrq');
INSERT INTO Production.product
(art_number, brand_id, description, name)
VALUES(44, 4, 'ghmsf', 'cwcw');
INSERT INTO Production.product
(art_number, brand_id, description, name)
VALUES(55, 5, 'yuoty', 'i[uop[');


INSERT INTO Production.brand
(name)
VALUES('asasd');
INSERT INTO Production.brand
(name)
VALUES('qweq');
INSERT INTO Production.brand
(name)
VALUES('zxczx');
INSERT INTO Production.brand
(name)
VALUES('hjkhj');
INSERT INTO Production.brand
(name)
VALUES('yuoyu');
