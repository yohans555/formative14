package com.example.demo.service;

import com.example.demo.model.Brand;
import com.example.demo.repository.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional
public class BrandService {
    @Autowired
    private BrandRepository brandRepository;
    public List<Brand> listAllbBrands() {
        return brandRepository.findAll();
    }

    public void saveBrand(Brand brand) {
        brandRepository.save(brand);
    }

    public Brand getBrand(Integer ID) {
        return brandRepository.findById(ID).get();
    }

    public void deleteBrand(Integer ID) {
        brandRepository.deleteById(ID);
    }
}
