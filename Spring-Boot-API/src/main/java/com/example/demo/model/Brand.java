package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "BRAND")
public class Brand {
    private int ID;
    private String NAME;

    public Brand() {
    }

    public Brand(int ID, String NAME) {
        this.ID = ID;
        this.NAME = NAME;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return ID;
    }

    public void setId(int ID) {
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }
}
