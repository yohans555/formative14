package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT")
public class Product {
    private int ID;
    private String NAME;
    private String DESCRIPTION;
    private int ART_NUMBER;
    private int BRAND_ID;


    public Product() {
    }

    public Product(int iD, String nAME, String dESCRIPTION, int aRT_NUMBER, int bRAND_ID) {
        ID = iD;
        NAME = nAME;
        DESCRIPTION = dESCRIPTION;
        ART_NUMBER = aRT_NUMBER;
        BRAND_ID = bRAND_ID;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String nAME) {
        NAME = nAME;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String dESCRIPTION) {
        DESCRIPTION = dESCRIPTION;
    }

    public int getART_NUMBER() {
        return ART_NUMBER;
    }

    public void setART_NUMBER(int aRT_NUMBER) {
        ART_NUMBER = aRT_NUMBER;
    }

    public int getBRAND_ID() {
        return BRAND_ID;
    }

    public void setBRAND_ID(int bRAND_ID) {
        BRAND_ID = bRAND_ID;
    }
}
