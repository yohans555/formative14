package com.example.demo.controller;

import com.example.demo.model.Brand;
import com.example.demo.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/Brands")
public class BrandController {
    @Autowired
    BrandService brandService;

    @GetMapping("")
    public List<Brand> list() {
        return brandService.listAllbBrands();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Brand> get(@PathVariable Integer id) {
        try {
            Brand brand = brandService.getBrand(id);
            return new ResponseEntity<Brand>(brand, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Brand>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/")
    public void add(@RequestBody Brand brand) {
        brandService.saveBrand(brand);
    }
    @PutMapping("/{ID}")
    public ResponseEntity<?> update(@RequestBody Brand brand, @PathVariable Integer id) {
        try {
            Brand existBrand = brandService.getBrand(id);
            brand.setId(id);
            brandService.saveBrand(brand);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{ID}")
    public void delete(@PathVariable Integer ID) {

        brandService.deleteBrand(ID);
    }
}
